#extends KinematicBody

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
#func _ready():
#	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

extends KinematicBody

# Member variables
var g = -1000
var vel = Vector3()
const MAX_SPEED = 5
const ACCEL= 2
const DEACCEL= 4

func _physics_process(delta):
	vel.y += delta * g
	vel = move_and_slide(vel, Vector3(0,1,0))


func _on_tcube_body_enter(body):
	if body == self:
		get_node("../ty").show()
