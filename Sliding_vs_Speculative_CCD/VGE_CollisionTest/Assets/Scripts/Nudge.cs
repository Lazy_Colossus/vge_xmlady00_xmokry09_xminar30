﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nudge : MonoBehaviour
{
    Ray ray;
    RaycastHit hit;

    private void Start()
    {
        Invoke("fly", 0.2f);
    }

    void fly ()
    {
        if (transform.name == "Ball2")
            GetComponent<Rigidbody>().AddForce(new Vector3(0f, 0f, 5000f));
    }

    void Update()
    {
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit))
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (hit.transform.name == "Cube")
                    hit.rigidbody.AddForceAtPosition(new Vector3(50000f, 0f, 0f), hit.point);

                if (hit.transform.name == "Ball")
                    hit.rigidbody.AddForce(new Vector3(-100f, 0f, 0f));

                if (hit.transform.name == "Ball2")
                    hit.rigidbody.AddForce(new Vector3(0f, 0f, 50000000f));

                //print(hit.collider.name);
            }
        }
    }

   /* void hitit()
    {
        hit.rigidbody.AddForceAtPosition(new Vector3(50000f, 0f, 0f), hit.point);
    }*/
}
