﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonStuff : MonoBehaviour
{

    public GameObject Ball;
    public GameObject Cube;

    public void ResetBallPos()
    {
        Ball.transform.position = new Vector3(23.6f, 1.25f, -3.6f);
        Ball.GetComponent<Rigidbody>().velocity = Vector3.zero;
        Ball.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
    }

    public void SetCCDD()
    {
        Application.LoadLevel(Application.loadedLevel);
    }

    public void SetCCDS()
    {
        Cube.GetComponent<Rigidbody>().collisionDetectionMode = CollisionDetectionMode.ContinuousSpeculative;
        ResetBallPos();
    }

    public void ExitApp()
    {
        Application.Quit();
    }
}
